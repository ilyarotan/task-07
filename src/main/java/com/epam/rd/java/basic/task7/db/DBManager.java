package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static Connection conn;
	private static Statement stmt;
	{
		try {
			//conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db?user=root&password=postgres");
			conn = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
			stmt = conn.createStatement();
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private static DBManager instance = new DBManager();

	public static synchronized DBManager getInstance() {
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userL = new ArrayList<>();
		try {
			ResultSet rs = stmt.executeQuery("SELECT * FROM users");
			while (rs.next()){
				Integer id = rs.getInt("id");
				String login = rs.getString("login");
				userL.add(new User(id, login));
			}
		}catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("fd",e);}
		return userL;
	}

	public boolean insertUser(User user) throws DBException {
		try {
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,user.getLogin());
			Integer n = stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			user.setId(rs.getInt(1));
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DBException("fd",e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean f = false;
		for (User user : users) {
			try {
				PreparedStatement stmt = conn.prepareStatement("DELETE FROM users WHERE id = ?");
				stmt.setInt(1, user.getId());
				if (stmt.execute())
					f = true;
				else f = false;
			} catch (Exception e) {
				e.printStackTrace();
				throw new DBException("fd",e);
			}
		}
		return f;
	}

	public User getUser(String login) throws DBException {
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM users WHERE login = ?", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,login);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return new User(rs.getInt("id"), rs.getString("login"));
			}
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DBException("fd",e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM teams WHERE name = ?", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,name);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return new Team(rs.getInt("id"), rs.getString("name"));
			}
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DBException("fd",e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamL = new ArrayList<>();
		try {
			ResultSet rs = stmt.executeQuery("SELECT * FROM teams");
			while (rs.next()){
				Integer id = rs.getInt("id");
				String login = rs.getString("name");
				teamL.add(new Team(id, login));
			}
		}catch (SQLException e) {e.printStackTrace();
			throw new DBException("fd",e);}
		return teamL;
	}

	public boolean insertTeam(Team team) throws DBException {
		try {
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,team.getName());
			Integer n = stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			team.setId(rs.getInt(1));
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DBException("fd",e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String sql = "INSERT INTO users_teams values(?,?)";
		int affectedRows = 0;
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try (
				PreparedStatement preparedStatement = connection.prepareStatement(sql);

		) {
			connection.setAutoCommit(false);
			preparedStatement.setInt(1, user.getId());

			for (Team team : teams) {
				preparedStatement.setInt(2, team.getId());
				affectedRows += preparedStatement.executeUpdate();
			}
			connection.commit();

		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException(e.getMessage(),e);

		}
		return affectedRows != 0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try {
			List <Team> teamL = new ArrayList<>();
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM users_teams WHERE user_id = ?");
			stmt.setInt(1,user.getId());
			ResultSet teamIdRS = stmt.executeQuery();
			while (teamIdRS.next()) {
				int teamId = teamIdRS.getInt("team_id");
				stmt = conn.prepareStatement("SELECT name FROM teams WHERE id = ?");
				stmt.setInt(1,teamId);
				ResultSet tNameRS = stmt.executeQuery();
				while (tNameRS.next()) {
					System.out.println(teamId);
					teamL.add(new Team(teamId, tNameRS.getString("name")));
				}
			}
			return teamL;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DBException("fd",e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
			try {
				PreparedStatement stmt = conn.prepareStatement("DELETE FROM teams WHERE id = ?");
				stmt.setInt(1,team.getId());
				if (stmt.execute())
					return true;
				else  return false;
			}catch (Exception e) {
				e.printStackTrace();
				throw new DBException("fd",e);
			}
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			PreparedStatement stmt = conn.prepareStatement(" UPDATE teams SET name = ? WHERE id = ? ");
			stmt.setString(1,team.getName());
			stmt.setInt(2,team.getId());
			if (stmt.execute())
				return true;
			else  return false;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DBException("fd",e);
		}
	}

}
